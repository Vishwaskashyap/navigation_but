package com.example.dell.navigation_a;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class Third_Page extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_third__page);

        Button button01 = (Button) findViewById(R.id.button_go_back_to_main_page3);
        button01.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.i("Go Back To Main page","-----View.OnClickListener reached ");

                //Intent intent = new Intent(getApplicationContext(),Main2Activity.class);
                //startActivity(intent);
                Intent intent1 = new Intent(getApplicationContext(),Main_Page.class);
                startActivity(intent1);
            }
        });

        Button button02 = (Button) findViewById(R.id.button_go_to_first_page3);
        button02.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.i("Go to First page","-----View.OnClickListener reached ");

                //Intent intent = new Intent(getApplicationContext(),Main2Activity.class);
                //startActivity(intent);
                Intent intent2 = new Intent(getApplicationContext(),First_Page.class);
                startActivity(intent2);
            }
        });

        Button button03 = (Button) findViewById(R.id.button_go_to_second_page3);
        button03.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.i("Go to Second Page","-----View.OnClickListener reached ");

                //Intent intent = new Intent(getApplicationContext(),Main2Activity.class);
                //startActivity(intent);
                Intent intent3 = new Intent(getApplicationContext(),Second_Page.class);
                startActivity(intent3);
            }
        });
    }
}
